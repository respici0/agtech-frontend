import {PROVISION_NEW_SENSOR} from "./types";

export const relateClusterToGraph = (cluster,type) => {
    return {
      type: type,
      payload: cluster
    };
};

export const provisionSensor = newSensor => {
  return {
    type: PROVISION_NEW_SENSOR,
    payload: newSensor
  };
};
