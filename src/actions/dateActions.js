import { SEARCH_THROUGH_DATES } from "./types";

export const updateSearchDates = (startDate, endDate) => {
  return {
    type: SEARCH_THROUGH_DATES,
    payload: [startDate, endDate]
  };
};
