import {
  FLY_TO_DEFAULT_LOCATION,
  SET_NORTHEAST_TRUE,
  SET_SOUTHWEST_TRUE,
  SET_COORDINATES,
  // SET_NEW_DEFAULT_LOCATION,
  SET_NORTHEAST_SOUTHWEST_FALSE
} from "./types";

export const setDefaultFarmLocation = farmCoordinates => {
  return {
    type: FLY_TO_DEFAULT_LOCATION,
    payload: farmCoordinates
  };
};

export const isNorthEastActive = northEast => {
  return {
    type: SET_NORTHEAST_TRUE,
    payload: northEast
  };
};

export const isSouthWestActive = southWest => {
  return {
    type: SET_SOUTHWEST_TRUE,
    payload: southWest
  };
};

export const setCoordinates = (
  longitude,
  latitude,
  isnorthEastActive,
  isSouthWestActive
) => {
  return {
    type: SET_COORDINATES,
    payload: {
      coordinates: [longitude, latitude],
      northEast: isnorthEastActive,
      southWest: isSouthWestActive
    }
  };
};

export const setNorthEastSouthWestFalse = () => {
  return {
    type: SET_NORTHEAST_SOUTHWEST_FALSE
  };
};
