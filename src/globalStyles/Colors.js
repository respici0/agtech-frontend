const Colors={
    coreColors:{
        gray1:"#DAD9DA",
        gray2:"#707070BF",
        gray3:"#4C5F5D",
        gray4:"#464646E6"
    },
    primaryColors:{
        white:"#FFFFFF",
        green:"#51A75A",
        blue:"#3F82FF",
        orange:"#FFA03F",
        watermelon:"#FF5F57",
        red:"#E14842",
        black:"#000000"

    }
}

export default Colors