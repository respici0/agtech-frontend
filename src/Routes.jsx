import React, { Suspense, lazy } from "react";
import { Route, Switch } from "react-router-dom";

const routes = [
  {
    name: "Dashbaord",
    path: "/",
    exact: true,
    Component: lazy(() => import("./components/Dashboard"))
  },
  {
    name: "Page not found",
    path: "*",
    exact: true,
    Component: lazy(() => import("./components/PageNotFound"))
  }
];

const renderRoutes = (route, index) => {
  return (
    <Route
      key={index}
      exact={route.exact}
      path={route.path}
      component={route.Component}
    />
  );
};

const Routes = () => {
  return (
    <Suspense fallback={"Loading"}>
      <Switch>{routes.map(renderRoutes)}</Switch>
    </Suspense>
  );
};

export default Routes;
