export default {
    type: "Cluster Collection",
    crs: { type: "name", properties: { name: "urn:ogc:def:crs:OGC:1.3:CRS84" } },
    features: [
      {
        type: "Cluster",
        clusterName: "Cluster 1",
        sensors:{
          air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
          soil:["Temperture","Moisture","NPK"]
        },
        coordinates: [-121.84, 42.03]
      },
      {
        type: "Cluster",
        clusterName: "Cluster 2",
        sensors:{
          air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
          soil:["Temperture","Moisture","NPK"]
        },
        coordinates: [ -121.81, 42.03]
      },
      {
        type: "Cluster",
        clusterName: "Cluster 3",
        sensors:{
          air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
          soil:["Temperture","Moisture","NPK"]
        },
        coordinates: [ -121.8, 42.01]
      },
      {
        type: "Cluster",
        clusterName: "Cluster 4",
        sensors:{
          air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
          soil:["Temperture","Moisture","NPK"]
        },
        coordinates: [-121.82,42.01]
      },
      {
        type: "Cluster",
        clusterName: "Cluster 5",
        sensors:{
          air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
          soil:["Temperture","Moisture","NPK"]
        },
        coordinates: [-121.76, 42.04]
      },
      {
        type: "Cluster",
        clusterName: "Cluster 6",
        sensors:{
          air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
          soil:["Temperture","Moisture","NPK"]
        },
        coordinates: [-121.78, 42]
      },
      {
        type: "Cluster",
        clusterName: "Cluster 7",
        sensors:{
          air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
          soil:["Temperture","Moisture","NPK"]
        },
        coordinates: [-121.83,42.04]
      },
      {
        type: "Cluster",
        clusterName: "Cluster 8",
        sensors:{
          air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
          soil:["Temperture","Moisture","NPK"]
        },
        coordinates: [-121.86, 42.02]
      },
      {
        type: "Cluster",
        clusterName: "Cluster 9",
        sensors:{
          air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
          soil:["Temperture","Moisture","NPK"]
        },
        coordinates: [ -121.79, 42.02]
      }
    ]
  };
  