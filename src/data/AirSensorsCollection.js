const airSensors = [
  {
    // label: "Air Temp",
    label: "Temperature",
    propertyName: "Air Temperature",
    unit: "Fahrenheit (F)"
  },
  {
    label: "Wind Speed",
    propertyName: "Wind Speed Sensor",
    unit: "Meters/Second (m/s)"
  },
  // {
  //   label: "Wind Direction",
  //   propertyName: "Wind Direction Sensor",
  //   unit:
  // },
  {
    label: "Humidity",
    propertyName: "Humidity",
    unit: "Percentage (%)"
  },
  {
    label: "Pressure",
    propertyName: "Barometric Pressure",
    unit: "hPa"
  },
  {
    label: "Rain",
    propertyName: "Rain Intensity",
    unit: "Percentage (%)"
  },
  {
    label: "Sunlight",
    propertyName: "Sunlight Intensity",
    unit: "Percentage (%)"
  },
  {
    label: "EVT",
    propertyName: "EVT Calculation",
    unit: "mm/day"
  }
];
export default airSensors;
