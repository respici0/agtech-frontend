export default {
  type: "Cluster Collection",
  crs: { type: "name", properties: { name: "urn:ogc:def:crs:OGC:1.3:CRS84" } },
  features: [
    {
      type: "Cluster",
      name: "Cluster 1",
      sensors:{
        air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
        soil:["Temperture","Moisture","NPK"]
      },
      properties: {
        "Soil Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [2, 10, 3, 4, 9]
        }, // potentially these will all be arrays
        "Soil Moisture": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [3, 7, 9, 2, 1]
        }, // potentially these will all be arrays
        "N.P.K": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [1, 2, 3, 4, 5]
        }, // potentially these will all be arrays
        "Wind Speed": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [2, 6, 3, 10, 13]
        }, // potentially these will all be arrays
        "Air Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [3, 7, 9, 2, 1]
        }, // potentially these will all be arrays
        "Relative Humidity": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [3, 7, 9, 2, 1]
        }, // potentially these will all be arrays
        "E.V.T": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [1, 0, 4, 0, 9]
        } // potentially these will all be arrays
      },
      coordinates: [-121.84, 42.03]
    },
    {
      type: "Cluster",
      name: "Cluster 2",
      sensors:{
        air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
        soil:["Temperture","Moisture","NPK"]
      },
      properties: {
        "Soil Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [3,4,2,5,5]
        }, // potentially these will all be arrays
        "Soil Moisture": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [6,3,6,4,2]
        },
        "N.P.K": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [6,4,7,4,3]
        },
        "Wind Speed": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [2,4,7,3,7]
        }, // potentially these will all be arrays
        "Air Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [4,6,3,8,6]
        }, // potentially these will all be arrays
        "Relative Humidity": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [7,3,6,4,3]
        }, // potentially these will all be arrays
        "E.V.T": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,5,8,5,3]
        } // potentially these will all be arrays
      },
      coordinates: [ -121.81, 42.03]
    },
    {
      type: "Cluster",
      name: "Cluster 3",
      sensors:{
        air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
        soil:["Temperture","Moisture","NPK"]
      },
      properties: {
        "Soil Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [6,3,7,4,2]
        }, // potentially these will all be arrays
        "Soil Moisture": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [6,4,7,5,3]
        },
        "N.P.K": {
          x: [],
          y: []
        },
        "Wind Speed": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [4,6,4,3,7]
        }, // potentially these will all be arrays
        "Air Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [2,4,7,8,10]
        }, // potentially these will all be arrays
        "Relative Humidity": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [6,3,8,4,8]
        }, // potentially these will all be arrays
        "E.V.T": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [6,9,6,3,7]
        } // potentially these will all be arrays
      },
      coordinates: [ -121.8, 42.01]
    },
    {
      type: "Cluster",
      name: "Cluster 4",
      sensors:{
        air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
        soil:["Temperture","Moisture","NPK"]
      },
      properties: {
        "Soil Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [4,4,6,8,5]
        }, // potentially these will all be arrays
        "Soil Moisture": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,6,4,6,8]
        },
        "N.P.K": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [5, 10, 11, 6,8]
        },
        "Wind Speed": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [4,7,9, 10, 8]
        }, // potentially these will all be arrays
        "Air Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [9,4,8,3,6]
        }, // potentially these will all be arrays
        "Relative Humidity": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,4,8,3,8]
        }, // potentially these will all be arrays
        "E.V.T": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,5,8,5,3]
        } // potentially these will all be arrays
      },
      coordinates: [-121.82,42.01]
    },
    {
      type: "Cluster",
      name: "Cluster 5",
      sensors:{
        air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
        soil:["Temperture","Moisture","NPK"]
      },
      properties: {
        "Soil Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [7,5,8,5,3]
        }, // potentially these will all be arrays
        "Soil Moisture": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,5,8,5,8]
        },
        "N.P.K": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [7,7,7,6,4]
        },
        "Wind Speed": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [6,7,8 ,10, 13]
        }, // potentially these will all be arrays
        "Air Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [3,5,6,4, 2]
        }, // potentially these will all be arrays
        "Relative Humidity": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [7,5,7,8,6]
        }, // potentially these will all be arrays
        "E.V.T": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [6,7,4,8, 9]
        } // potentially these will all be arrays
      },
      coordinates: [-121.76, 42.04]
    },
    {
      type: "Cluster",
      name: "Cluster 6",
      sensors:{
        air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
        soil:["Temperture","Moisture","NPK"]
      },
      properties: {
        "Soil Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [6,4,7,8, 3]
        }, // potentially these will all be arrays
        "Soil Moisture": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [6,5,7,10, 13]
        },
        "N.P.K": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [5,5,5,7, 9]
        },
        "Wind Speed": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,9,9,5, 13]
        }, // potentially these will all be arrays
        "Air Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,5,8,6,5,2]
        }, // potentially these will all be arrays
        "Relative Humidity": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,5,8,5, 3]
        }, // potentially these will all be arrays
        "E.V.T": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [4,4,2,7, 6]
        } // potentially these will all be arrays
      },
      coordinates: [-121.78, 42]
    },
    {
      type: "Cluster",
      name: "Cluster 7",
      sensors:{
        air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
        soil:["Temperture","Moisture","NPK"]
      },
      properties: {
        "Soil Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [5,4,6,7, 9]
        }, // potentially these will all be arrays
        "Soil Moisture": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [7,7,5,8, 8]
        },
        "N.P.K": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [9,9,8,7, 1]
        },
        "Wind Speed": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [7,9,5,8, 13]
        }, // potentially these will all be arrays
        "Air Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [9,1,5,7, 2]
        }, // potentially these will all be arrays
        "Relative Humidity": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [7,5,8,4, 8]
        }, // potentially these will all be arrays
        "E.V.T": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,5,8,4, 8]
        } // potentially these will all be arrays
      },
      coordinates: [-121.83,42.04]
    },
    {
      type: "Cluster",
      name: "Cluster 8",
      sensors:{
        air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
        soil:["Temperture","Moisture","NPK"]
      },
      properties: {
        "Soil Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [7,5,7,5, 11]
        }, // potentially these will all be arrays
        "Soil Moisture": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,3,6,8, 11]
        },
        "N.P.K": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,5,7,5, 9]
        },
        "Wind Speed": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [7,5,7,5, 13]
        }, // potentially these will all be arrays
        "Air Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,6,8,6, 2]
        }, // potentially these will all be arrays
        "Relative Humidity": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,4,6,8, 1]
        }, // potentially these will all be arrays
        "E.V.T": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,5,6,3, 9]
        } // potentially these will all be arrays
      },
      coordinates: [-121.86, 42.02]
    },
    {
      type: "Cluster",
      name: "Cluster 9",
      sensors:{
        air:["Temperature","Wind Speed","Wind Direction","Humidity","Rain","Pressure","Sunlight","EVT"],
        soil:["Temperture","Moisture","NPK"]
      },
      properties: {
        "Soil Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [9,5,4,7, 2]
        }, // potentially these will all be arrays
        "Soil Moisture": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,5,3,7, 0]
        },
        "N.P.K": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [6,4,8,4, 2]
        },
        "Wind Speed": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,5,5,4, 13]
        }, // potentially these will all be arrays
        "Air Temperature": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [7,5,8,5,4]
        }, // potentially these will all be arrays
        "Relative Humidity": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,6,8,5,8]
        }, // potentially these will all be arrays
        "E.V.T": {
          x: [
            "2019-11-11",
            "2019-11-12",
            "2019-11-13",
            "2019-11-14",
            "2019-11-15"
          ],
          y: [8,6,9,7,5]
        } // potentially these will all be arrays
      },
      coordinates: [ -121.79, 42.02]
    }
  ]
};
