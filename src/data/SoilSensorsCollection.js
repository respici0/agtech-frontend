const soilSensors = [
  {
    // label: "Soil Temp",
    label: "Temperature",
    propertyName: "Soil Temperature",
    unit: "Fahrenheit (F)"
  },
  {
    label: "Moisture",
    // label: "Soil Moisture",
    propertyName: "Soil Moisture",
    unit: "Percentage (%)"
  },
  {
    label: "Nitrogen",
    propertyName: "Nitrogen",
    unit: "mg/kg"
  },
  {
    label: "Phosphorus",
    propertyName: "Phosphorus",
    unit: "mg/kg"
  },
  {
    label: "Potassium",
    propertyName: "Potassium",
    unit: "mg/kg"
  }
];
export default soilSensors;
