import { makeStyles } from "@material-ui/core/styles";

const ChartStyle = makeStyles(theme => ({
  chart: {
    height: "20vh",
    width: "100%"
  }
}));

export default ChartStyle;
