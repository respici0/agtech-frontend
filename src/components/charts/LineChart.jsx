import React from "react";
import Plotly from "plotly.js";
import ChartStyle from "./ChartStyle";
import createPlotlyComponent from "react-plotly.js/factory";
import { useSelector } from "react-redux";
import SelectedMarkerColorPlate from "../maps/SelectedMarkerColorPlate";
import SelectedMinMarkerColorPlate from "../maps/SelectedMinMarkerColorPlate";
import SelectedMaxMarkerColorPlate from "../maps/SelectedMaxMarkerColorPlate";

const Plot = createPlotlyComponent(Plotly);

export const LineChart = props => {
  const classes = ChartStyle();
  const [startDate, endDate] = useSelector(state => state.dates);
  const selectedClusters = useSelector(state => state.clusters);
  const isYminPresent =
    props.data[0] === undefined
      ? {}
      : props.data[0].sensors.soil["Soil Temperature"].hasOwnProperty("ymin");
  const isYmaxPresent =
    props.data[0] === undefined
      ? {}
      : props.data[0].sensors.soil["Soil Temperature"].hasOwnProperty("ymax");

  const getColorIndex = clusterId => {
    var index = -1;
    selectedClusters.map((cluster, idx) => {
      return cluster.clusterId === clusterId ? (index = idx) : null;
    });
    return index;
  };

  const dateFormatrer = arr => {
    var newDates = [];
    arr.map(ar => newDates.push(dateConverter(ar)));
    return newDates;
  };
  const dateConverter = timeStamp => {
    var date = new Date(timeStamp * 1000);
    var year = date.getFullYear();
    var m = date.getMonth() + 1;
    var month = m < 10 ? "0" + m : m;
    var day = date.getDate();
    var h = date.getHours();
    var hour = h < 10 ? "0" + h : h;
    var min = date.getMinutes();
    var minute = min < 10 ? "0" + min : min;
    var s = date.getSeconds();
    var second = s < 10 ? "0" + s : s;
    var formattedDate =
      year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
    return formattedDate;
  };

  // const selectUnit = () => {

  // };
  // selectUnit();

  const customRange = {
    xaxis: {
      type: "dateTime",
      range: [startDate, endDate],
      autosize: true
    },
    // yaxis: {
    //   autorange: true,
    //   type: "linear",
    //   autosize: true
    // },
    yaxis: {
      title: {
        text: `${props.unit}`,
        font: {
          family: "Raleway",
          size: 12,
          color: "#7f7f7f"
        }
      },
      autorange: true,
      type: "linear",
      autosize: true
    },
    margin: {
      r: 20,
      t: 0,
      b: 35,
      l: 35,
      pad: 0
    },
    autosize: true,
    showlegend: true
  };

  const yAxis =
    props.data.length && isYminPresent && isYmaxPresent
      ? props.data.map((cluster, idx) => {
          let { sensors, clusterName } = cluster;
          console.log(sensors);
          let propertyName = props.propertyName;
          let key = Object.keys(sensors[props.type])[0];
          let x = dateFormatrer(sensors[props.type][key].x);
          var colorIndex = getColorIndex(cluster.clusterId);
          return {
            x: x.length ? x : [],
            y: sensors[props.type][propertyName]
              ? sensors[props.type][propertyName].y
              : [],
            mode: "lines",
            fill: "tonexty",
            legendgroup: `group${colorIndex}`,
            line: {
              color: SelectedMarkerColorPlate[colorIndex]
            },
            fillcolor: SelectedMinMarkerColorPlate[colorIndex],
            name: `${clusterName}`
          };
        })
      : props.data.map((cluster, idx) => {
          let { sensors, clusterName } = cluster;
          // console.log(sensors);
          let propertyName = props.propertyName;
          let key = Object.keys(sensors[props.type])[0];
          let x = dateFormatrer(sensors[props.type][key].x);
          var colorIndex = getColorIndex(cluster.clusterId);
          return {
            x: x.length ? x : [],
            y: sensors[props.type][propertyName]
              ? sensors[props.type][propertyName].y
              : [],
            mode: "lines",
            legendgroup: `group${colorIndex}`,
            line: {
              color: SelectedMarkerColorPlate[colorIndex]
            },
            name: `${clusterName}`
          };
        });

  const yMin = props.data.length
    ? props.data.map((cluster, idx) => {
        let { sensors } = cluster;
        console.log(sensors);
        let propertyName = props.propertyName;
        let key = Object.keys(sensors[props.type])[0];
        let x = dateFormatrer(sensors[props.type][key].x);
        var colorIndex = getColorIndex(cluster.clusterId);
        return {
          x: sensors[props.type][propertyName] ? x : [],
          y: sensors[props.type][propertyName]
            ? sensors[props.type][propertyName].ymin
            : [],
          mode: "lines",
          showlegend: false,
          legendgroup: `group${colorIndex}`,

          line: {
            color: SelectedMarkerColorPlate[colorIndex]
          },
          name: `Min`
        };
      })
    : {};

  const yMax = props.data.length
    ? props.data.map((cluster, idx) => {
        let { sensors } = cluster;
        console.log(sensors);
        let propertyName = props.propertyName;
        let key = Object.keys(sensors[props.type])[0];
        let x = dateFormatrer(sensors[props.type][key].x);
        var colorIndex = getColorIndex(cluster.clusterId);
        return {
          x: sensors[props.type][propertyName] ? x : [],
          y: sensors[props.type][propertyName]
            ? sensors[props.type][propertyName].ymax
            : [],
          mode: "lines",
          fill: "tonexty",
          showlegend: false,
          legendgroup: `group${colorIndex}`,
          line: {
            color: SelectedMarkerColorPlate[colorIndex]
          },
          fillcolor: SelectedMaxMarkerColorPlate[colorIndex],
          name: `Max`
        };
      })
    : {};

  var mergedYAxis = [].concat.apply([], [yMin, yAxis, yMax]);
  console.log(mergedYAxis);

  return (
    <Plot
      className={classes.chart}
      data={isYminPresent && isYmaxPresent ? mergedYAxis : yAxis}
      layout={customRange}
      useResizeHandlers={true}
      config={{ displayModeBar: false }}
    />
  );
};

export default LineChart;
