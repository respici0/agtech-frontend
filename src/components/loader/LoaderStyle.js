import { makeStyles } from "@material-ui/core/styles";

const LoaderStyle=makeStyles(theme => ({
    loaderContainer: {
        display:"flex",
        justifyContent:"center"
    }
  }));

export default LoaderStyle