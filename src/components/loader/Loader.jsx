import React from 'react'
import Loader from 'react-loader-spinner'
import Color from '../../globalStyles/Colors'
import LoaderStyle from "./LoaderStyle"

const LoaderThreeDots=(props)=>{
    const classes=LoaderStyle()
    return(
        <div className={classes.loaderContainer}>
            <Loader type="ThreeDots" color={Color.primaryColors.green} height={150} width={80}/>
        </div>
    )
}
export default LoaderThreeDots