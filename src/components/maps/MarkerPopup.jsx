import React from "react";
import { Popup } from "react-mapbox-gl";
import Button from "@material-ui/core/Button";
import CloseIcon from "@material-ui/icons/Close";
import MapBoxStyle from "./MapBoxStyle";
import "./MarkerPopup.css";

const MarkerPopup = props => {
  const classes = MapBoxStyle();
  return (
    <Popup coordinates={props.popupInfo.coordinates} anchor="center">
      <div className={classes.popupCloseIconContainer} onClick={props.handlePopupClose}>
        <CloseIcon className={classes.popupCloseIcon} />
      </div>
      <div className={classes.popupContainer}>
        <p className={classes.popupTitle}>{props.popupInfo.clusterName}</p>
        <small>
          {props.popupInfo.coordinates[0]}, {props.popupInfo.coordinates[1]}
        </small>
        <br />
        <p className={classes.popupSubtitle}>Sensors</p>
        <p>Air</p>
        <div>{props.popupInfo.sensors.air.map(sensor => sensor + ", ")}</div>
        <p>Soil</p>
        <div>{props.popupInfo.sensors.soil.map(sensor => sensor + ", ")}</div>
        {props.isSelected ? (
          <Button
            variant="contained"
            className={classes.removeClusterFromGraphBtn}
            onClick={props.removeClusterFromGraphBtnOnClick}
          >
            Remove Cluster From Graph
          </Button>
        ) : (
          <Button
            variant="contained"
            className={classes.addClusterToGraphBtn}
            onClick={props.addClusterToGraphBtnOnClick}
          >
            Add Cluster To Graph
          </Button>
        )}
      </div>
    </Popup>
  );
};
export default MarkerPopup;
