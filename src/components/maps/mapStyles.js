export const sateliteView = "mapbox://styles/mapbox/satellite-v9";
export const streetView = "mapbox://styles/mapbox/streets-v10";
export const lightView = "mapbox://styles/mapbox/light-v9";
export const darkView = "mapbox://styles/mapbox/dark-v10";
export const outdoors = "mapbox://styles/mapbox/outdoors-v11";
