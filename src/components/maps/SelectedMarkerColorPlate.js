const SelectedMarkerColorPlate = [
  "#90c6d1",
  "#E0A218",
  "#FF5F57",
  "#778868",
  "#6cc4ad",
  "#d1c994",
  "#e6a3c8",
  "#90c6d1",
  "#13AD2D"
];

export default SelectedMarkerColorPlate;
