import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { roundTwoDecimals } from "utils/roundTwoDecimals";

const useStyles = makeStyles(theme => ({
  longLatStyle: {
    padding: 0,
    margin: 0
  }
}));

export const CoordinateInfo = ({ longitude, latitude }) => {
  const classes = useStyles();
  return (
    <div>
      <p className={classes.longLatStyle}>
        Longitude: {roundTwoDecimals(longitude)}
      </p>
      <p className={classes.longLatStyle}>
        Latitude: {roundTwoDecimals(latitude)}
      </p>
    </div>
  );
};

CoordinateInfo.propTypes = {
  longitude: PropTypes.number.isRequired,
  latitude: PropTypes.number.isRequired
};
