export const SelectedMaxMarkerColorPlate = [
  `rgba(144, 198, 209, 0.7)`,
  `rgba(224, 162, 24, 0.7)`,
  `rgba(255, 95, 87, 0.7)`,
  `rgba(119, 136, 104, 0.7)`,
  `rgba(119, 136, 104, 0.7)`,
  `rgba(209, 201, 148, 0.7)`,
  `rgba(230, 163, 200, 0.7)`,
  `rgba(144, 198, 209, 0.7)`,
  `rgba(19, 173, 45, 0.7)`
];

export default SelectedMaxMarkerColorPlate;
