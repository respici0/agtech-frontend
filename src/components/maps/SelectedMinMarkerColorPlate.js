export const SelectedMinMarkerColorPlate = [
  `rgba(144, 198, 209, 0.5)`,
  `rgba(224, 162, 24, 0.5)`,
  `rgba(255, 95, 87, 0.5)`,
  `rgba(119, 136, 104, 0.5)`,
  `rgba(119, 136, 104, 0.5)`,
  `rgba(209, 201, 148, 0.5)`,
  `rgba(230, 163, 200, 0.5)`,
  `rgba(144, 198, 209, 0.5)`,
  `rgba(19, 173, 45, 0.5)`
];

export default SelectedMinMarkerColorPlate;
