import { makeStyles } from "@material-ui/core/styles";
import Colors from "../../globalStyles/Colors";

const MapBoxStyle = makeStyles(theme => ({
  popUpInfo: {
    background: "rgba(0,0,0,0.5)",
    color: "#fff",
    position: "absolute",
    bottom: 95,
    width: "8rem",
    left: 10,
    padding: ".5rem",
    fontSize: "12px"
  },
  longLatInfo: {
    bottom: 40
  },
  navigation: {
    position: "absolute",
    right: 5,
    top: 5
  },
  inputDisplay: {
    display: "flex",
    flexDirection: "row"
  },
  boundingPin: {
    cursor: "pointer",
    fill: "#d00",
    stroke: "none"
  },
  chartContainer: {
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    paddingRight: 10
  },
  addClusterToGraphBtn: {
    color: "white",
    width: "100%",
    height: 33,
    borderRadius: 0,
    marginTop: "10px",
    backgroundColor: Colors.primaryColors.green,
    "&:hover": {
      opacity: 0.9,
      backgroundColor: Colors.primaryColors.green
    }
  },
  removeClusterFromGraphBtn: {
    color: "white",
    width: "100%",
    height: 33,
    borderRadius: 0,
    marginTop: "10px",
    backgroundColor: Colors.primaryColors.red,
    "&:hover": {
      opacity: 0.9,
      backgroundColor: Colors.primaryColors.red
    }
  },
  popupContainer: {
    width: 300
  },
  popupTitle: {
    fontSize: "20px",
    margin: 0
  },
  popupSubtitle: {
    fontSize: "18px",
    marginTop: 10
  },
  navigationControl: {
    position: "absolute",
    bottom: 95,
    left: 0,
    padding: "10px"
  },
  popupCloseIconContainer: {
    height: 20,
    textAlign: "right",
    "&:hover": {
      cursor: "pointer"
    }
  },
  popupCloseIcon: {
    width: 20,
    "&:focus,&:hover": {
      backgroundColor: Colors.coreColors.gray1,
      borderRadius: 5
    }
  },
  addClusterBrn: {
    position: "absolute",
    top: 70,
    left: 12,
    backgroundColor: "white",
    borderRadius: 2,
    cursor: "pointer"
  },
  checkboxLabel: {
    fontSize: "10px"
  },
  select: {
    position: "absolute",
    padding: 0,
    backgroundColor: "white",
    left: 50,
    top: 10,
    borderRadius: 3,
    width: "150px",
    height: "30px",
    paddingLeft: 10,
    cursor: "pointer"
  }
}));
export default MapBoxStyle;
