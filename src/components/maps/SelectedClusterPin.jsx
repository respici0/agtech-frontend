import React from "react";
import PropTypes from "prop-types";
import RoomIcon from "@material-ui/icons/Room";

const ClusterPin = ({ size = 40, onClick, color }) => (
  <RoomIcon
    style={{
      cursor: "pointer",
      fill: color,
      stroke: "none",
      width: 40,
      height: 40
      // transform: `translate(${-size / 2}px,${-size}px)`
    }}
    onClick={onClick}
  />
);

ClusterPin.propTypes = {
  size: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired
};

export default ClusterPin;
