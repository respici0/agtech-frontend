import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import ReactMapboxGl, {
  ZoomControl,
  Marker,
  Cluster,
  Popup
} from "react-mapbox-gl";
import SelectedClusterPin from "./SelectedClusterPin";
import DefaultClusterPin from "./DefaultClusterPin";
import SelectedMarkerColorPlate from "./SelectedMarkerColorPlate";
import API from "service/API";
import {
  ADD_CLUSTER,
  REMOVE_CLUSTER,
  CLEAR_CLUSTER
} from "../../actions/types";
import { relateClusterToGraph } from "actions/sensorActions";
import CardWithChip from "../card/CardWithChip";
import MarkerPopup from "./MarkerPopup";
import { ChartTabs } from "../navigation/ChartTabs";
import { sateliteView } from "./mapStyles";
import MapBoxStyle from "./MapBoxStyle";
import CreateClusterIcon from "@material-ui/icons/AddLocation";
import AirSensors from "data/AirSensorsCollection";
import SoilSensors from "data/SoilSensorsCollection";
import CreateClusterPopup from "./CreateClusterPopup";
import addDays from "date-fns/addDays";
import "./MarkerPopup.css";

// const TOKEN = process.env.REACT_APP_MAPBOX_ACCESS_TOKEN;
// const Mapbox = ReactMapboxGl({ accessToken: TOKEN });

const Mapbox = ReactMapboxGl({
  accessToken:
    "pk.eyJ1IjoiY2FtaXdpbnMiLCJhIjoiY2s5ZDkxcDE2MDE4aTNwczZwYWZ6ZG1maSJ9.sVXGukRs9HmRLI2U7kJOjg"
});

export const Map = () => {
  const classes = MapBoxStyle();
  const dispatch = useDispatch();
  const [farmId, setfarmId] = useState(112);
  const defaultMapCenter = [-122.151545, 41.144585];
  const [mapZoom, setMapZoom] = useState([17]);
  const [farmName, setfarmName] = useState("");
  const [farms, setFarms] = useState([]);
  const mapTheme = sateliteView;
  const [mapCenter, setMapCenter] = useState(defaultMapCenter);
  const [clusters, setclusters] = useState([]);
  const [activeClusterId, setActiveClusterId] = useState(-1);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const selectedClusters = useSelector(state => state.clusters);
  const [startDate, endDate] = useSelector(state => state.dates);
  const provisionClusterInitialState = {
    clusterName: "",
    farmName: "",
    ipAddress: "",
    port: "",
    longtitude: "",
    latitude: ""
  };
  const [provisionCluster, setProvisionCluster] = useState(
    provisionClusterInitialState
  );
  const [clusterData, setClusterData] = useState([]);
  const [chartLoading, setChartLoading] = useState(false);
  // ==================get data for charts=====================
  const comparer = otherArray => {
    return function (current) {
      return (
        otherArray.filter(function (other) {
          return Number(other.clusterId) === Number(current.clusterId);
        }).length === 0
      );
    };
  };
  useEffect(() => {
    if (clusterData.length - selectedClusters.length === 1) {
      var removedCluster = clusterData.filter(comparer(selectedClusters));
      var removedIndex = clusterData.indexOf(removedCluster[0]);
      var arr = clusterData.slice();
      arr.splice(removedIndex, 1);
      setClusterData(arr);
    } else if (selectedClusters.length - clusterData.length === 1) {
      setChartLoading(true);
      var formattedStartDate = new Date(startDate).getTime() / 1000;
      // var formattedEndDate = new Date(endDate).getTime() / 1000;
      var formattedEndDate = addDays(new Date(endDate), 1).getTime() / 1000;
      API.getClusterData(
        farmId,
        selectedClusters.slice(-1)[0].clusterId,
        formattedStartDate,
        formattedEndDate,
        getClusterDataOnSuccess1
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedClusters.length]);

  useEffect(() => {
    selectedClusters.length && setChartLoading(true);
    selectedClusters.length &&
      selectedClusters.map(cluster => {
        var formattedStartDate = new Date(startDate).getTime() / 1000;
        // var formattedEndDate = new Date(endDate).getTime() / 1000;
        var formattedEndDate = addDays(new Date(endDate), 1).getTime() / 1000;
        // console.log("==formattedEndDate==", formattedEndDate);
        API.getClusterData(
          farmId,
          cluster.clusterId,
          formattedStartDate,
          formattedEndDate,
          getClusterDataOnSuccess2
        );
        return cluster;
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [startDate, endDate]);

  const getClusterDataOnSuccess1 = resp => {
    console.log(resp);
    var arr = clusterData.slice();
    arr.push(resp.data.data[0]);
    setClusterData(arr);
    setChartLoading(false);
  };

  var newClusterData = [];
  const getClusterDataOnSuccess2 = resp => {
    newClusterData.push(resp.data.data[0]);
    setClusterData(newClusterData);
    newClusterData.length === clusterData.length && setChartLoading(false);
  };

  // ===============orginaze data for provisioning=====================
  const handleInputsChange = e => {
    var key = e.target.name;
    var val = e.target.value;
    setProvisionCluster({ ...provisionCluster, [key]: val });
  };
  const handleInputsBlur = e => {
    var key = e.target.name;
    var val = e.target.value;
    setProvisionCluster({ ...provisionCluster, [key]: val });
  };

  var provisionSensors = { air: [], soil: [] };
  const handleCheckboxBlur = e => {
    const category = e.currentTarget.getAttribute("category");
    if (
      e.target.checked &&
      !provisionSensors[category].includes(e.target.value)
    ) {
      provisionSensors[category].push(e.target.value);
    } else {
      var index = provisionSensors[category].indexOf(e.target.value);
      provisionSensors[category].splice(index, 1);
    }
  };
  //=======device provisioning==============
  const handleCreateClusterClick = e => {
    setAnchorEl(null);
    var data = {
      farmName: farmName,
      farmId: farmId,
      clusterName: provisionCluster.clusterName,
      ipAddress: provisionCluster.ipAddress,
      port: provisionCluster.port,
      longtitude: provisionCluster.longtitude,
      latitude: provisionCluster.latitude,
      sensors: provisionSensors
    };
    API.createNewCluster(data, createNewClusterOnSuccess);
  };

  const createNewClusterOnSuccess = resp => {
    provisionClusterInitialState.farmName = farmName;
    provisionClusterInitialState.farmId = farmId;
    setProvisionCluster(provisionClusterInitialState);
    API.getClusters(farmId, getClustersOnSuccess);
    alert("New cluster is provisioned successfully!");
  };
  //==========get cluster data for the map==================
  useEffect(() => {
    setClusterData([]);
    dispatch(relateClusterToGraph({}, CLEAR_CLUSTER));
    API.getClusters(farmId, getClustersOnSuccess);
    API.getAllFarms(getAllFarmsOnSuccess);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [farmId]);

  const getAllFarmsOnSuccess = resp => {
    setFarms(resp.data.farms);
    var currentFarm = resp.data.farms.filter(
      farm => Number(farm.farmId) === farmId
    );
    setfarmName(currentFarm[0].farm_name);
    setProvisionCluster({
      ...provisionCluster,
      farmName: currentFarm[0].farm_name
    });
  };

  const getClustersOnSuccess = resp => {
    setclusters(resp.data.data);
  };
  //=========render cluster pins on the map==============
  const renderClusterPin = (cluster, index) => {
    var i = selectedClusters.findIndex(
      selectedCluster => selectedCluster.clusterId === cluster.clusterId
    );
    return (
      <>
        <Marker
          key={`marker-${index}`}
          coordinates={cluster.coordinates}
          className="markerOne"
        >
          {i >= 0 ? (
            <SelectedClusterPin
              color={SelectedMarkerColorPlate[i]}
              size={25}
              onClick={() => clusterOnClick(cluster)}
            />
          ) : (
              <DefaultClusterPin
                size={25}
                onClick={() => clusterOnClick(cluster)}
              />
            )}
        </Marker>
        <Marker
          key={`marker-${index}`}
          coordinates={cluster.coordinates}
          className="markerTwo"
        >
          {parseInt(cluster.clusterId) === parseInt(activeClusterId) &&
            renderPopup()}
        </Marker>
      </>
    );
  };
  const clusterOnClick = cluster => {
    cluster.isSelected = true;
    setActiveClusterId(cluster.clusterId);
  };
  //================marker popup=========================

  const renderPopup = () => {
    const cluster = clusters.filter(
      cluster => parseInt(cluster.clusterId) === parseInt(activeClusterId)
    )[0]
      ? clusters.filter(
        cluster => parseInt(cluster.clusterId) === parseInt(activeClusterId)
      )[0]
      : {};

    return (
      parseInt(cluster.clusterId) === parseInt(activeClusterId) && (
        <MarkerPopup
          coordinates={cluster.coordinates}
          isSelected={selectedClusters.includes(cluster)}
          popupInfo={cluster}
          handlePopupClose={handlePopupClose}
          addClusterToGraphBtnOnClick={() =>
            addClusterToGraphBtnOnClick(cluster)
          }
          removeClusterFromGraphBtnOnClick={() =>
            removeClusterFromGraphBtnOnClick(cluster)
          }
        />
      )
    );
  };

  const handlePopupClose = () => {
    setActiveClusterId(-1);
  };
  //====================add or remove cluster from graph==================
  const addClusterToGraphBtnOnClick = cluster => {
    dispatch(relateClusterToGraph(cluster, ADD_CLUSTER));
    setActiveClusterId(-1);
  };

  const removeClusterFromGraphBtnOnClick = cluster => {
    dispatch(relateClusterToGraph(cluster, REMOVE_CLUSTER));
    //force component to update, may change later
    activeClusterId === -1 ? setActiveClusterId(-2) : setActiveClusterId(-1);
  };

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleFarmChange = e => {
    var targetfarmId = Number(e.target.value);
    var currentFarm = farms.filter(
      farm => Number(farm.farmId) === targetfarmId
    );
    setfarmName(currentFarm[0].farm_name);
    setProvisionCluster({
      ...provisionCluster,
      farmName: currentFarm[0].farm_name
    });
    setfarmId(targetfarmId);
    if (targetfarmId === 112) {
      setMapCenter([-122.151545, 41.144585]);
      setMapZoom([17]);
    } else if (targetfarmId === 111) {
      setMapCenter([-122.094753, 41.23984]);
      setMapZoom([15]);
    }
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  return (
    <Mapbox
      style={mapTheme}
      zoom={mapZoom}
      containerStyle={{
        height: "87vh", // 87vh originally
        width: "100%"
      }}
      // renderChildrenInPortal={true}
      center={mapCenter}
      movingMethod={"easeTo"}
    >
      {clusters.map(renderClusterPin)}
      {/* {renderPopup()} */}

      <div>
        <CreateClusterIcon
          className={classes.addClusterBrn}
          aria-describedby={id}
          onClick={handleClick}
        />

        <CreateClusterPopup
          id={id}
          open={open}
          anchorEl={anchorEl}
          farmName={farmName}
          handleClose={handleClose}
          provisionCluster={provisionCluster}
          handleInputsChange={handleInputsChange}
          handleInputsBlur={handleInputsBlur}
          handleCheckboxBlur={handleCheckboxBlur}
          handleCreateClusterClick={handleCreateClusterClick}
        />
        <select
          className={classes.select}
          value={farmId}
          onChange={handleFarmChange}
          disabled={chartLoading}
        >
          {farms &&
            farms.map((farm, key) => {
              return (
                <option key={key} value={farm.farmId}>
                  {farm.farm_name}
                </option>
              );
            })}
        </select>
      </div>

      <div className={classes.chartContainer}>
        <CardWithChip
          selectedClusters={selectedClusters}
          handleClusterChipDelete={removeClusterFromGraphBtnOnClick}
        />
        <ChartTabs
          tabs={AirSensors}
          data={clusterData}
          chartType="air"
          loading={chartLoading}
        />
        <ChartTabs
          tabs={SoilSensors}
          data={clusterData}
          chartType="soil"
          loading={chartLoading}
        />
      </div>
      <ZoomControl position="top-left" />
    </Mapbox>
  );
};
