import React from "react";
import MapBoxStyle from "./MapBoxStyle";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Popover from "@material-ui/core/Popover";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import AirSensors from "data/AirSensorsCollection";
import SoilSensors from "data/SoilSensorsCollection";

const CreateClusterPopup = props => {
  const classes = MapBoxStyle();
  return (
    <Popover
      id={props.id}
      open={props.open}
      anchorEl={props.anchorEl}
      onClose={props.handleClose}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "right"
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "left"
      }}
    >
      <div style={{ padding: 15, width: 500 }}>
        <p className={classes.popupTitle}>Create New Cluster</p>
        <small>Place the pin on the map where the new cluster will be located</small>
        <Grid container direction="row" justify="center" alignItems="flex-start" spacing={2}>
          <Grid item xs={4}>
            <form>
              <TextField
                id="standard-basic"
                label="Cluster Name"
                name="clusterName"
                defaultValue={props.provisionCluster.clusterName}
                onBlur={props.handleInputsBlur}
              />
              <TextField
                id="standard-basic"
                label="Farm Name"
                name="farmName"
                defaultValue={props.provisionCluster.farmName}
                onBlur={props.handleInputsBlur}
              />
              <TextField
                id="standard-basic"
                label="IPv4 Address"
                name="ipAddress"
                defaultValue={props.provisionCluster.ipAddress}
                onBlur={props.handleInputsBlur}
              />
              <TextField
                id="standard-basic"
                label="Port"
                name="port"
                defaultValue={props.provisionCluster.port}
                onBlur={props.handleInputsBlur}
              />
              <TextField
                id="standard-basic"
                type="number"
                label="Longtitude"
                name="longtitude"
                defaultValue={props.provisionCluster.longtitude}
                onBlur={props.handleInputsBlur}
              />
              <TextField
                id="standard-basic"
                type="number"
                label="Latitude"
                name="latitude"
                defaultValue={props.provisionCluster.latitude}
                onBlur={props.handleInputsBlur}
              />
            </form>
          </Grid>
          <Grid item xs={8}>
            <p className={classes.popupSubtitle}>Sensors</p>
            <Grid container direction="row" justify="center" alignItems="flex-start" spacing={0}>
              <Grid item xs={6}>
                <div style={{ paddingLeft: 10 }}>Air</div>
                {AirSensors.map((sensor, index) => (
                  <div
                    key={index}
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "flex-start",
                      height: 27
                    }}
                  >
                    <Checkbox
                      defaultChecked={false}
                      category="air"
                      onBlur={props.handleCheckboxBlur}
                      value={sensor.propertyName}
                    />
                    <div>{sensor.label}</div>
                  </div>
                ))}
              </Grid>
              <Grid item xs={6}>
                <div style={{ paddingLeft: 10 }}>Soil</div>
                {SoilSensors.map((sensor, index) => (
                  <div
                    key={index}
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "flex-start",
                      height: 27
                    }}
                  >
                    <Checkbox
                      defaultChecked={false}
                      category="soil"
                      onBlur={props.handleCheckboxBlur}
                      value={sensor.propertyName}
                    />
                    <div>{sensor.label}</div>
                  </div>
                ))}
              </Grid>
            </Grid>
          </Grid>
          <Button
            variant="contained"
            className={classes.addClusterToGraphBtn}
            onClick={props.handleCreateClusterClick}
          >
            Create Cluster
          </Button>
        </Grid>
      </div>
    </Popover>
  );
};
export default CreateClusterPopup;
