import React, { useState, useEffect } from "react";
import "date-fns";
import subDays from "date-fns/subDays";
import addDays from "date-fns/addDays";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider, KeyboardDatePicker } from "@material-ui/pickers";
import Button from "@material-ui/core/Button";
import { useSelector, useDispatch } from "react-redux";
import { updateSearchDates } from "actions/dateActions";
import { formatDate } from "utils/dateFormat";
import { dateIsValid } from "utils/dateIsValid";
import Grid from "@material-ui/core/Grid";
import DatePickerStyle from "./DatePckerStyle";

export const DatePicker = props => {
  const classes = DatePickerStyle();
  const dispatch = useDispatch();

  const [startDate, endDate] = useSelector(state => state.dates);
  const [selectedStartDate, setselectedStartDate] = useState(subDays(new Date(), 7));
  const [selectedEndDate, setselectedEndDate] = useState(new Date());
  const [validStartDate, setvalidStartDate] = useState(false);
  const [validEndDate, setvalidEndDate] = useState(false);

  useEffect(() => {
    if (startDate && endDate) {
      setselectedStartDate(new Date(startDate));
      setselectedEndDate(new Date(endDate));
    }
  }, [startDate, endDate]);

  const handleStartDateChange = startDate => {
    if (!dateIsValid(startDate)) {
      setselectedStartDate(startDate);
      setvalidStartDate(true);
    } else {
      setselectedStartDate(startDate);
      setvalidStartDate(false);
    }
  };

  const handleEndDateChange = endDate => {
    if (!dateIsValid(endDate)) {
      setselectedEndDate(endDate);
      setvalidEndDate(true);
    } else {
      setselectedEndDate(endDate);
      setvalidEndDate(false);
    }
  };

  const searchSelectedDates = () => {
    dispatch(updateSearchDates(formatDate(selectedStartDate), formatDate(selectedEndDate)));
    props.handleClose();
  };

  return (
    <div>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <Grid container spacing={1} direction="row" justify="center">
          <Grid item xs={12} className={classes.gridItem}>
            <KeyboardDatePicker
              className={classes.dateStart}
              invalidDateMessage="Insert valid format: MM/dd/yyyy"
              autoOk="true"
              variant="inline"
              format="MM/dd/yyyy"
              margin="normal"
              label="Start Date"
              value={selectedStartDate}
              maxDate={selectedEndDate}
              onChange={handleStartDateChange}
            />
          </Grid>
          <Grid item xs={12} className={classes.gridItem}>
            <KeyboardDatePicker
              className={classes.dateEnd}
              invalidDateMessage="Insert valid format: MM/dd/yyyy"
              disableFuture="true"
              autoOk="true"
              variant="inline"
              format="MM/dd/yyyy"
              margin="normal"
              label="End Date"
              value={selectedEndDate}
              minDate={selectedStartDate}
              onChange={handleEndDateChange}
            />
          </Grid>
        </Grid>
      </MuiPickersUtilsProvider>
      <Button
        variant="contained"
        className={classes.button}
        onClick={searchSelectedDates}
        disabled={validStartDate || validEndDate ? true : false}
      >
        Update
      </Button>
    </div>
  );
};
