import { makeStyles } from "@material-ui/core/styles";
import Colors from "../../globalStyles/Colors"

const DatePickerStyle = makeStyles(theme => ({
    gridItem:{
      display:"flex",
      justifyContent:"center"
    },
    button: {
      color:"white",
      width:"100%",
      height:50,
      borderRadius:0,
      marginTop: "2rem",
      backgroundColor:Colors.primaryColors.green,
      "&:hover":{
        opacity: 0.9,
        backgroundColor:Colors.primaryColors.green
      }
    }
  }));

  export default DatePickerStyle