import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { Map } from "./maps/MapBox";
import TopNavBar from "./navigation/TopNavBar";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    margin: "1rem"
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  }
}));

const Dashboard = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <TopNavBar />
          <Map />
        </Grid>
      </Grid>
    </div>
  );
};
export default Dashboard;
