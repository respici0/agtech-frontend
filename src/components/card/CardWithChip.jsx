import React from "react";
import { Card, CardHeader, CardContent } from "@material-ui/core";
import Chip from "@material-ui/core/Chip";
import CardStyle from "./CardStyle";
import SelectedMarkerColorPlate from "../maps/SelectedMarkerColorPlate";
// import { useSelector } from "react-redux";

const CardWithChip = props => {
  const classes = CardStyle();
  // var selectedClusters=useSelector(state=>state.clusters)
  const cardTitle = "Selected Sensor Cluster(s)";
  const cardSusheader =
    "Select a marker on the map to add a cluster of the sensors to the graph below";
  return (
    <div className={classes.cardContainer}>
      <Card className={classes.card}>
        <CardHeader
          classes={{
            root: classes.cardHeaderRoot,
            title: classes.title,
            subheader: classes.subheader
          }}
          title={cardTitle}
          subheader={cardSusheader}
          disableTypography={false}
        />
        <CardContent classes={{ root: classes.cardContentRoot }}>
          {props.selectedClusters.length ? (
            props.selectedClusters.map((cluster, index) => {
              return (
                <Chip
                  style={{ backgroundColor: SelectedMarkerColorPlate[index] }}
                  classes={{ root: classes.chipRoot }}
                  key={index}
                  label={cluster.clusterName}
                  onDelete={() => {
                    props.handleClusterChipDelete(cluster);
                  }}
                />
              );
            })
          ) : (
            <p className={classes.clusterNotSelectedText}>No clusters selected yet</p>
          )}
        </CardContent>
      </Card>
    </div>
  );
};
export default CardWithChip;
