import { makeStyles } from "@material-ui/core/styles";

const CardStyle = makeStyles(theme => ({
  cardContainer: {
    height: "19%",
    width: "50%",
    marginTop: 10,
    zIndex: 10
  },
  cardHeaderRoot: {
    padding: "5px 10px",
    fontFamily: "Raleway"
  },
  title: {
    fontSize: "24px",
    fontFamily: "Raleway"
  },
  subheader: {
    fontSize: "16px",
    fontFamily: "Roboto",
    letterSpaciing: 0,
    fontWeight: 300,
    opacity: 0.5
  },
  card: {
    height: "100%"
  },
  cardContentRoot: {
    padding: "2px 10px",
    fontFamily: "Raleway"
  },
  chipRoot: {
    fontSize: 10,
    margin: "2px",
    color: "white",
    width: "23%"
  },
  clusterNotSelectedText: {
    fontSize: "16px",
    fontFamily: "Roboto",
    letterSpaciing: 0,
    fontWeight: 300,
    opacity: 0.5
  }
}));

export default CardStyle;
