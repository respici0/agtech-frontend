import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { withStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Button from "@material-ui/core/Button";
import { Auth } from "aws-amplify";
import CalendarIcon from "@material-ui/icons/EventOutlined";
import LogOutIcon from "@material-ui/icons/DirectionsRun";
import NavigationStyles from "./NavigationStyle";
import Popover from "@material-ui/core/Popover";
import { DatePicker } from "../datepicker/DatePicker";
import { formatToFullDate } from "utils/dateFormat";
import { useSelector } from "react-redux";

const ToolBarWithoutGutter = withStyles({
  gutters: {
    paddingLeft: "0px",
    paddingRight: "0px"
  }
})(Toolbar);

export default function TopNavBar(props) {
  const classes = NavigationStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [targetedPopoverId, setTargetedPopoverId] = React.useState(null);
  const date = useSelector(reduxState => reduxState.dates);
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  const logOut = e => {
    Auth.signOut();
  };

  const handleClick = event => {
    setTargetedPopoverId(event.currentTarget.id);
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const setPopoverContent = popoverId => {
    switch (popoverId) {
      case "calenderIcon":
        return <DatePicker handleClose={handleClose} />;
      case "settingIcon":
        return <div>setting</div>;
      case "userIcon":
        return <div>userIcon</div>;
      default:
        return null;
    }
  };

  console.log(new Date(date[0]));

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar className={classes.appBar}>
        <ToolBarWithoutGutter>
          <h3 className={classes.title}>{"AgTech"}</h3>
          {/* <p>Viewing clusters from January 2019 - December 2019</p> */}
          <p className={classes.dateText}>
            Viewing clusters from {formatToFullDate(new Date(date[0]))} -{" "}
            {formatToFullDate(new Date(date[1]))}
          </p>
          <div className={classes.iconButtonContainer}>
            {/* ===========popover==================== */}
            <Popover
              id={id}
              open={open}
              anchorEl={anchorEl}
              onClose={handleClose}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "center"
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "center"
              }}
            >
              <div style={{ width: 300 }}>
                {targetedPopoverId && setPopoverContent(targetedPopoverId)}
              </div>
            </Popover>
            {/* =================================== */}

            <Button
              className={classes.iconButton}
              id="calenderIcon"
              aria-describedby={id}
              onClick={handleClick}
            >
              <CalendarIcon className={classes.rightIcon} />
            </Button>
            <Button
              className={classes.iconButton}
              id="userIcon"
              aria-describedby={id}
              onClick={logOut}
            >
              <LogOutIcon className={classes.rightIcon} />
            </Button>
          </div>
        </ToolBarWithoutGutter>
      </AppBar>
      <Toolbar id="back-to-top-anchor" />
    </React.Fragment>
  );
}
