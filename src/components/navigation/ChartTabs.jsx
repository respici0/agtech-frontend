import React, { useState, Suspense } from "react";
import PropTypes from "prop-types";
import SwipeableViews from "react-swipeable-views";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import LineChart from "../charts/LineChart";
import Loader from "../loader/Loader";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";

const useStyles = makeStyles(themes => ({
  root: {
    backgroundColor: themes.palette.background.paper,
    width: "50%",
    height: "38%",
    fontFamily: "Raleway",
    marginTop: themes.spacing(1),
    borderRadius: 6,
    zIndex: 10
  },
  chartName: {
    margin: "1rem 0 0 1rem",
    fontFamily: "Raleway",
    fontSize: "1.5rem",
    fontWeight: 400,
    letterSpacing: 0,
    color: " #464646",
    opacity: 1
  },
  tabText: {
    fontFamily: "Raleway",
    fontSize: "1rem",
    fontWeight: 600
  }
}));

const theme = createMuiTheme({
  palette: {
    primary: { main: "#51A75A" },
    secondary: { main: "#51A75A" }
  }
});

const TabPanel = props => {
  const { children, value, index, ...other } = props;
  console.log(children.props.propertyName);
  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
};

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`
  };
}

export const ChartTabs = props => {
  const classes = useStyles();
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  // console.log(props.tabs);

  return (
    <ThemeProvider theme={theme}>
      <div className={classes.root}>
        <AppBar position="static" color="default">
          {props.chartType === "air" ? (
            <p className={classes.chartName}>AIR</p>
          ) : (
            <p className={classes.chartName}>SOIL</p>
          )}
          {/* <p style={{ marginLeft: "40px" }}>AIR</p> */}
          <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor={"primary"}
            textColor={"primary"}
            variant="scrollable"
            scrollButtons="auto"
            aria-label="full width tabs example"
          >
            {props.tabs.map((tab, index) => (
              <Tab
                className={classes.tabText}
                key={index}
                label={tab.label}
                {...a11yProps(index)}
              />
            ))}
          </Tabs>
        </AppBar>
        <Suspense fallback={"Loading"}>
          <SwipeableViews axis={theme.direction === "rtl" ? "x-reverse" : "x"} index={value}>
            {props.tabs.map((tab, index) => (
              <TabPanel key={index} value={value} index={value} dir={theme.direction}>
                {!props.loading ? (
                  <LineChart
                    propertyName={tab.propertyName}
                    unit={tab.unit}
                    data={props.data}
                    type={props.chartType}
                  />
                ) : (
                  <Loader />
                )}
              </TabPanel>
            ))}
          </SwipeableViews>
        </Suspense>
      </div>
    </ThemeProvider>
  );
};
