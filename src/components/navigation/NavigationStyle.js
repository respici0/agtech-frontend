import { makeStyles } from "@material-ui/core/styles";
import Colors from "../../globalStyles/Colors";

const NavigationStyle = makeStyles(theme => ({
  root: {
    position: "fixed",
    bottom: theme.spacing(2),
    right: theme.spacing(2)
  },
  appBar: {
    //   background: `linear-gradient(45deg, rgba(243, 243, 243, 1) 60%, ${Colors.coreColors.gray2} 90%)`,
    background: Colors.primaryColors.white,
    color: "rgba(85, 91, 110, 1)",
    boxShadow: "0px"
  },
  title: {
    width: "40%",
    textAlign: "left",
    fontSize: "calc(20px + 1vw)",
    paddingLeft: "5%",
    margin: 0,
    color: Colors.primaryColors.green
  },
  dateText: {
    width: "40%",
    fontWeight: 600,
    letterSpacing: 0,
    color: Colors.primaryColors.black
  },
  rightIcon: {
    width: 25,
    height: 25,
    marginLeft: theme.spacing(0)
  },
  iconButton: {
    color: Colors.coreColors.gray3
  },
  iconButtonContainer: {
    width: "20%",
    paddingRight: "5%",
    textAlign: "right"
  }
}));

export default NavigationStyle;
