import axios from "axios";

const GetClustersUrl =
  "https://ooitww9gtb.execute-api.us-east-1.amazonaws.com/production/ddb?com=";

class API {
  static getClusters(farmId, onSuccess) {
    axios.get(`${GetClustersUrl}getClusters&fid=${farmId}`).then(onSuccess);
  }
  static getClusterData(farmId, clusterId, startDate, endDate, onSuccess) {
    axios
      .get(
        `${GetClustersUrl}getData&fid=${farmId}&cid=${clusterId}&start=${startDate}&end=${endDate}`
      )
      .then(onSuccess);
  }
  static createNewCluster(data, onSuccess) {
    axios.post(`${GetClustersUrl}putCluster`, data).then(onSuccess);
  }
  static getAllFarms(onSuccess) {
    axios.get(`${GetClustersUrl}getFarms`).then(onSuccess);
  }
}

export default API;
