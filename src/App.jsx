import React from "react";
import Routes from "./Routes";
import "./App.css";
// import NavigationAppBar from "./components/navigation/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
// import { withAuthenticator } from "aws-amplify-react";

function App() {
  return (
    <>
      <CssBaseline />
      <Routes />
    </>
  );
}

// export default withAuthenticator(App, {
//   signUpConfig: {
//     signUpFields: [
//       {
//         label: "Name",
//         key: "name",
//         required: true,
//         displayOrder: 3,
//         type: "string"
//       }
//     ]
//   }
// });

export default App;
