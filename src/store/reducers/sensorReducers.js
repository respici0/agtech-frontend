import {
  ADD_CLUSTER,
  REMOVE_CLUSTER,
  CLEAR_CLUSTER
} from "actions/types";

export const relateClusterReducer = (state = [], action) => {
  switch (action.type) {
    case ADD_CLUSTER:
      state.includes(action.payload)
        ? (state = [...state])
        : (state = [...state, action.payload]);
      return state;
    case REMOVE_CLUSTER:
        if(state.includes(action.payload)){
          var i=state.indexOf(action.payload)
          state.splice(i,1)
          return state
        }
        else return state;
    case CLEAR_CLUSTER:
      return [];
    default:
      return state;
  }
};