import {
  FLY_TO_DEFAULT_LOCATION,
  SET_NORTHEAST_TRUE,
  SET_SOUTHWEST_TRUE,
  SET_COORDINATES,
  SET_NORTHEAST_SOUTHWEST_FALSE
} from "actions/types";

export const flyToDefaultReducer = (state = [], action) => {
  switch (action.type) {
    case FLY_TO_DEFAULT_LOCATION:
      return [...action.payload];
    default:
      return state;
  }
};

export const setInputActiveReducer = (
  state = { northEastActive: false, southWestActive: false },
  action
) => {
  switch (action.type) {
    case SET_NORTHEAST_TRUE:
      return { northEastActive: action.payload, southWestActive: false };
    case SET_SOUTHWEST_TRUE:
      return { northEastActive: false, southWestActive: action.payload };
    case SET_NORTHEAST_SOUTHWEST_FALSE:
      return { northEastActive: false, southWestActive: false };
    default:
      return state;
  }
};

export const setBoundingBoxReducer = (
  state = {
    northEast: [-121.77, 42.03],
    southWest: [-121.86, 42.01],
    zoom: 0
  },
  action
) => {
  switch (action.type) {
    case SET_COORDINATES:
      // use if/else using northEast or southWest and return the state with coordinates
      if (action.payload.northEast) {
        state = { ...state, northEast: action.payload.coordinates };
      } else if (action.payload.southWest) {
        state = { ...state, southWest: action.payload.coordinates };
      }
      return state;
    // case SET_NEW_DEFAULT_LOCATION:
    //   return (state = { ...state, zoom: parseInt(action.payload) });
    default:
      return state;
  }
};
