import { SEARCH_THROUGH_DATES } from "actions/types";
import subDays from "date-fns/subDays";
import addDays from "date-fns/addDays";
import { formatDate } from "utils/dateFormat";

export const searchDateReducer = (state = [], action) => {
  switch (action.type) {
    case SEARCH_THROUGH_DATES:
      return [...action.payload];

    default:
      if (!state.length) {
        var today = new Date();
        var startDate = subDays(new Date(), 7);
        var defaultStartDate = formatDate(startDate);
        var defaultEndDate = formatDate(today);
        state = [defaultStartDate, defaultEndDate];
        return state;
      } else {
        return state;
      }
  }
};
