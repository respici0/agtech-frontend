import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import reduxThunk from "redux-thunk";
import {
  relateClusterReducer,
  // updateSensorNameReducer
} from "./reducers/sensorReducers";
import { searchDateReducer } from "./reducers/dateReducers";
import {
  flyToDefaultReducer,
  setInputActiveReducer,
  setBoundingBoxReducer
} from "./reducers/mapReducers";

const rootReducer = combineReducers({
  clusters: relateClusterReducer,
  // sensorNames: updateSensorNameReducer,
  dates: searchDateReducer,
  map: flyToDefaultReducer,
  isActive: setInputActiveReducer,
  boundingBox: setBoundingBoxReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(reduxThunk))
);
