import { isValid } from "date-fns";

export const dateIsValid = date => isValid(date);
