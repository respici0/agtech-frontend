export const roundTwoDecimals = number => Math.round(number * 100) / 100;
