import { format } from "date-fns";

export const formatDate = date => format(date, "MM/dd/yyyy");

export const formatToFullDate = date => format(date, "MMMM do, yyyy");
